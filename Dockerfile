FROM node:10.17.0-alpine

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh

ADD package.json /tmp/package.json
RUN cd /tmp && npm install --production
RUN npm install --production

RUN mkdir -p /usr/src/app
RUN mkdir /root/.ssh 
# RUN chmod 0700 /root/.ssh/
RUN ssh-keyscan -t rsa bitbucket.org >> /root/.ssh/known_hosts
ADD start.sh /start.sh
WORKDIR /usr/src/app

EXPOSE 5050
CMD [ "sh", "/start.sh" ]
