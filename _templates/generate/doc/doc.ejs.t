---
to: docs/<%= h.inflection.camelize(name, true) %>.yaml
---
paths:
  /<%= h.inflection.camelize(name, true) %>:
    get:
      tags:
        - <%= h.inflection.camelize(name, false) %>
      summary: Get List <%= h.inflection.camelize(name) %>
      description: Lấy thông tin dữ liệu <%= h.inflection.camelize(name) %>
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
      security:
        - api-key: []
    post:
      tags:
        - <%= h.inflection.camelize(name, false) %>
      summary: Create <%= h.inflection.camelize(name) %>
      description: Tạo mới <%= h.inflection.camelize(name) %>
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/<%= h.inflection.camelize(name) %>"
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
      security:
        - api-key: []
    delete:
      tags:
        - <%= h.inflection.camelize(name, false) %>
      summary: Delete Many <%= h.inflection.camelize(name) %>
      description: Xóa nhiều <%= h.inflection.camelize(name) %>
      parameters:
        - name: ids
          in: query
          description: List Ids
          required: true
          schema:
            type: array
            items:
              type: string
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
      security:
        - api-key: []
  /<%= h.inflection.camelize(name, true) %>/count:
    get:
      tags:
        - <%= h.inflection.camelize(name, false) %>
      summary: Count <%= h.inflection.camelize(name) %>
      description: Đếm số lượng record <%= h.inflection.camelize(name) %>
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
      security:
        - api-key: []
  /<%= h.inflection.camelize(name, true) %>/{id}:
    get:
      tags:
        - <%= h.inflection.camelize(name, false) %>
      summary: Get One <%= h.inflection.camelize(name) %>
      description: Lấy thông tin một dữ liệu dữ liệu <%= h.inflection.camelize(name) %>
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
      security:
        - api-key: []
    put:
      tags:
        - <%= h.inflection.camelize(name, false) %>
      summary: Update <%= h.inflection.camelize(name) %>
      description: Chỉnh sửa <%= h.inflection.camelize(name) %>
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/<%= h.inflection.camelize(name) %>"
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
      security:
        - api-key: []
    delete:
      tags:
        - <%= h.inflection.camelize(name, false) %>
      summary: Delete One <%= h.inflection.camelize(name) %>
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
      security:
        - api-key: []
components:
  schemas:
    <%= h.inflection.camelize(name) %>:
      type: object
      properties:
        name:
          type: string