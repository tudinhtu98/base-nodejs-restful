const inquirer = require('inquirer');
const generator = require('custom-template-generator');
const fs = require('fs');
const _ = require('lodash');
const DIR = process.env['PWD'];
const path = require('path');
const options = [
  { id: 'full-model', action: createFullModel },
  { id: 'job', action: createJob },
  { id: 'service', action: createService },
  { id: 'middlewares', action: createMiddleware },
  { id: 'controller', action: createController },
  { id: 'model-service', action: createServiceModel },
];
const questions = [
  {
    type: 'list',
    name: 'template',
    message: 'Generate Option?',
    choices: options.map((o) => o.id),
  },
];

async function run() {
  const { template } = await inquirer.prompt(questions);
  const option = _.find(options, (o) => o.id == template);
  if (!option) return;
  option.action();
}

async function createServiceModel() {
  const { name, version, table } = await inquirer.prompt([
    { type: 'input', name: 'name', message: 'file name?' },
    { type: 'input', name: 'version', message: 'version?', default: 'v1' },
    // { type: 'list', name: 'engine', message: 'engine?', choices: ['sqlite','mongo','oracle'], default: 'mongo' },
    // { type: 'input', name: 'table', message: 'table?' }
  ]);
  const result = generator({
    componentName: _.camelCase(name),
    customTemplatesUrl: './tools/templates', // path.join(DIR,'tools/templates'),
    dest: `./src/${version}`, // path.join(DIR,'src',version),
    templateName: 'model-service',
    wrapInFolder: false,
    data: { engine: 'postgres', name: _.camelCase(name), table },
  });
}

async function createFullModel() {
  const { name, version, table } = await inquirer.prompt([
    { type: 'input', name: 'name', message: 'file name?' },
    { type: 'input', name: 'version', message: 'version?', default: 'v1' },
    // { type: 'list', name: 'engine', message: 'engine?', choices: ['sqlite','mongo','oracle'], default: 'mongo' },
    // { type: 'input', name: 'table', message: 'table?' }
  ]);
  const result = generator({
    componentName: _.camelCase(name),
    customTemplatesUrl: './tools/templates', // path.join(DIR,'tools/templates'),
    dest: `./src/${version}`, // path.join(DIR,'src',version),
    templateName: 'model',
    wrapInFolder: false,
    data: { engine: 'postgres', name: _.camelCase(name), table },
  });
}

function appendLine(file, sign, line) {
  return new Promise((resolve, reject) => {
    fs.readFile(file, 'utf8', function (err, data) {
      if (err) {
        reject(err);
        return console.log(err);
      }
      const regex = new RegExp(`(\/\/ \<${sign}\>)`, 'g');
      var result = data.replace(regex, `${line}\n$1`);

      fs.writeFile(file, result, 'utf8', function (err) {
        if (err) {
          reject(err);
          return console.log(err);
        }
        resolve();
      });
    });
  });
}

async function createJob() {
  var { name, version, cron } = await inquirer.prompt([
    { type: 'input', name: 'name', message: 'file name?' },
    { type: 'input', name: 'version', message: 'version?', default: 'v2' },
    { type: 'input', name: 'cron', message: 'cron string?' },
  ]);
  var humanToCron = require('human-to-cron');
  cron = humanToCron(cron);
  const result = generator({
    componentName: _.camelCase(name),
    customTemplatesUrl: './tools/templates', // path.join(DIR,'tools/templates'),
    dest: `./src/${version}`, // path.join(DIR,'src',version),
    templateName: 'job',
    wrapInFolder: false,
    data: { cron },
  });
}

async function createService() {
  const { name, version } = await inquirer.prompt([
    { type: 'input', name: 'name', message: 'service name?' },
    { type: 'input', name: 'version', message: 'version?', default: 'v1' },
  ]);

  const result = generator({
    componentName: _.camelCase(name),
    customTemplatesUrl: './tools/templates', // path.join(DIR,'tools/templates'),
    dest: `./src/${version}`, // path.join(DIR,'src',version),
    templateName: 'service',
    wrapInFolder: false,
    data: { name: _.camelCase(name) },
  });
}

async function createMiddleware() {
  const { name, version } = await inquirer.prompt([
    { type: 'input', name: 'name', message: 'middleware name?' },
    { type: 'input', name: 'version', message: 'version?', default: 'v1' },
  ]);

  const result = generator({
    componentName: _.camelCase(name),
    customTemplatesUrl: './tools/templates', // path.join(DIR,'tools/templates'),
    dest: `./src/${version}`, // path.join(DIR,'src',version),
    templateName: 'middleware',
    wrapInFolder: false,
    data: { name: _.camelCase(name) },
  });
}

async function createController() {
  const { name, version } = await inquirer.prompt([
    { type: 'input', name: 'name', message: 'controller name?' },
    { type: 'input', name: 'version', message: 'version?', default: 'v1' },
  ]);
  const result = generator({
    componentName: _.camelCase(name),
    customTemplatesUrl: './tools/templates', // path.join(DIR,'tools/templates'),
    dest: `./src/${version}`, // path.join(DIR,'src',version),
    templateName: 'controller',
    wrapInFolder: false,
    data: { name: _.camelCase(name) },
  });
}

run();
