import { BaseCrudRouter } from "../../core/adapter/mongo/crud/baseCrudRouter";
import { {name}Controller, {Name}Controller } from "../controllers/crud/{name}";
import { authMiddleware, queryMiddleware } from "../../core/middlewares";

export default class {Name}Router extends BaseCrudRouter<{Name}Controller> {
    constructor() {
        super({name}Controller);
    }
    customRouting() {
    }

    countMiddlewares(): any[] { return [authMiddleware.run([]), queryMiddleware.run()] }
    getListMiddlewares(): any[] { return [authMiddleware.run([]), queryMiddleware.run()] }
    getItemMiddlewares(): any[] { return [authMiddleware.run([]), queryMiddleware.run()] }
    createMiddlewares(): any[] { return [authMiddleware.run([])] }
    updateMiddlewares(): any[] { return [authMiddleware.run([])] }
    deleteMiddlewares(): any[] { return [authMiddleware.run([])] }
    deleteAllMiddlewares(): any[] { return [authMiddleware.run([])] }
}