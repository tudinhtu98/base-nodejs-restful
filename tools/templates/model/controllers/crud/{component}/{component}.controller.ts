import { BaseCrudController } from "../../../../core/adapter/mongo/crud/baseCrudController";
import { {Name}Service, {name}Service } from "../../../services/crud/{name}";
import { Request } from "../../../../core/interfaces";

export class {Name}Controller extends BaseCrudController<{Name}Service> {
    constructor() {
        super({name}Service);
    }
    async appendCreateData(req: Request) {
        return { };
    }
    async appendUpdateData(req: Request) {
        return { };
    }
}