import { {Name}Controller } from './{name}.controller';

const {name}Controller = new {Name}Controller();

export { {Name}Controller, {name}Controller }