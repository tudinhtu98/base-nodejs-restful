import { BaseDocument } from "../../core/adapter/mongo/baseModel";
import  mongoose from "mongoose"
// import  mongooseHidden from 'mongoose-hidden'
import { MainConnection } from './connections/main';

const Schema = mongoose.Schema

export type {Name}Document = BaseDocument & {

}

const {name}Schema = new Schema({

    status: { type: String, enum: ["active", "deactive"], default: "active" }
}, { timestamps: true })

// {name}Schema.plugin(mongooseHidden({ defaultHidden: { _id: false } }))

export let {Name}Model: mongoose.Model<{Name}Document> = MainConnection.model('{Name}', {name}Schema);