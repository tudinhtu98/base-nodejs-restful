import { BaseMiddleware } from "../../../core/baseMiddleware";
import { NextFunction, Request, Response } from "../../../core/interfaces";
export class {Name}Middleware extends BaseMiddleware {
    async use(req: Request, res: Response, next: NextFunction) {
        next();
    }
}