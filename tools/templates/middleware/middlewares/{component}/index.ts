import { {Name}Middleware } from "./{name}.middleware";

const {name}Middleware = new {Name}Middleware();

export { {name}Middleware, {Name}Middleware }