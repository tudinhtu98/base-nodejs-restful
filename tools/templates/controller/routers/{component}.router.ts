import { BaseRouter } from '../../core/baseRouter';
import { {name}Controller, {Name}Controller } from "../controllers/{name}";
export default class {Name}Router extends BaseRouter<{Name}Controller> {
    constructor() {
        super();
        this.controller = {name}Controller;
    }
    controller: {Name}Controller;
}
