import { Job } from "../job";

export default class {Name}Job extends Job {
    constructor() { 
        super(`{cron}`)
        this.active = false;  
    }

    async run() {
        console.log('running job {Name}Job');
    }
}