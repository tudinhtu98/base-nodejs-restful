import { {Name}Service } from "./{name}.service";

const {name}Service = new {Name}Service();

export { {name}Service, {Name}Service }