import { BaseCrudService } from "../../../../core/adapter/mongo/crud/baseCrudService";
import { {Name}Model, {Name}Document } from "../../../models/{name}.model";

export class {Name}Service extends BaseCrudService<{Name}Document> {
    constructor() {
        super({Name}Model);
        console.log('contructor {Name}Service')
    }
}

