const { issueService } = require("../dev/v1/services/crud/issue");
const { userService } = require("../dev/v1/services/crud/user");

async function updateUser() {
    const users = await userService.model.aggregate([
        {
            $match: {
                type: "member"
            }
        },
        {
            $lookup: {
                from: "issues",
                localField: "_id",
                foreignField: "owner",
                as: "issue"
            }
        },
        {
            $project: {
                issueCount: {$size: "$issue"}
            }
        }
    ]);
    console.log("users.length", users.length);
    console.log("users[0]",users[0]._id.toString());
    const bulk = userService.model.collection.initializeUnorderedBulkOp();
    for (let u of users) {
        bulk.find({ _id: u._id}).updateOne({
            $set: {issueCount:  u.issueCount}
        });
    }
    const bulkResult = await bulk.execute().catch(err => console.log('Lỗi bulk execute', err));
    console.log("Đã update issueCount cho user thành công", bulkResult);
}

updateUser();