var Rsync = require('rsync');
var shell = require('shelljs');
// Build the command
var argv = require('minimist')(process.argv.slice(2));
var dest = argv.prod ? 'dist' : 'dev';
var rsync = Rsync.build({
  source: ['./src/public', './src/templates'],
  destination: './' + dest,
  flags: 'avh',
  delete: true
});

// Execute the command
rsync.execute(function(error, code, cmd) {
  console.log('cmd', cmd);
  if (error) {
    console.log('ERROR', error.message, code, cmd);
    shell.cp('-Rf', 'src/templates', dest + '/');
    shell.cp('-Rf', 'src/public', dest + '/');
  } else {
    console.log('DONE');
  }
});
