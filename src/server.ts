import bodyParser from 'body-parser';
import compression from 'compression';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';
import logger from 'morgan';
import path from 'path';

import { config } from './config';
import v1 from './v1/routers';
import swaggerUi from 'swagger-ui-express';
import { swaggerSpec, swaggerTheme } from './config/swagger';

console.log('Starting server with at ' + process.pid + ' on port ' + config.port);
/**
 * Express configuration.
 */
const app = express();
app.use(
  logger('common', {
    skip: function (req: any, res: any) {
      if (req.url == '/_ah/health') {
        return true;
      } else {
        return false;
      }
    },
  })
);
app.use(
  bodyParser.json({
    limit: '50mb',
  })
);
app.use(
  bodyParser.urlencoded({
    extended: false,
    limit: '50mb',
  })
);
app.use(cookieParser());

// set the view engine to ejs
app.set('view engine', 'ejs');
app.use(compression());
app.use('/public', express.static(path.join(__dirname, '../public')));
app.set('views', path.join(__dirname, '../public/templates'));

app.set('port', config.port);
app.get('/api/version', function (request, response) {
  response.send('version 2.0.0');
  response.end();
});
// @ts-ignore
app.use('/api/*', cors());
app.use('/api/v1', v1);

if (config.debug) {
  app.get('/login', (req, res) => {
    res.render('login');
  });
  app.get('/loginEmail', (req, res) => {
    res.render('loginEmail');
  });
  app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec, { customCss: swaggerTheme }));
  app.get('/swagger.json', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
  });
  app.get('/redoc', (req, res) => {
    res.render('redoc');
  });
}

app.listen(app.get('port'), function () {
  console.log(`${config.name} started at ${config.domain}`);
});
