export interface ICrudOption {
    [x: string]: any,

    filter?: any
    limit?: any
    offset?: number
    fields?: any[]
    populates?: any
    lean?: boolean
    transaction?: any
    order?: any
    attributes?: any
    includes?: any
    paranoid?: any
    group?: any,
    cached?: boolean,
    allowNull?: boolean,

    language?: any
}
export interface ICrudExecOption {
    allowNull?: boolean
}