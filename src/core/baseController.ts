import _ from 'lodash';
import { ICrudOption, IValidateSchema } from './interfaces';
import { Response } from './interfaces/routerInterface';
import { utilService, errorService } from './services';

export class BaseController {
  protected pageSize = 25;
  onSuccess(res: Response, object: any = {}, extras: any = {}) {
    if (object === undefined) object = {};
    res.json({
      code: 200,
      data: _.merge(object, extras),
    });
  }
  onSuccessAsList(res: Response, objects: any = [], total: number, option?: ICrudOption) {
    option = option || {};
    if (objects.toJSON) {
      objects = objects.toJSON();
    }
    const page = _.floor((option.offset || 0) / (option.limit || this.pageSize)) + 1;
    res.json({
      code: 200,
      data: objects,
      pagination: {
        currentPage: page,
        nextPage: page + 1,
        prevPage: page - 1,
        limit: option.limit,
        totalItems: total || 0,
      },
    });
  }
  async validateJSON(body: any, schema: IValidateSchema) {
    const validate = utilService.validateJSON(schema, body);
    if (!validate.isValid) {
      throw errorService.requestDataInvalid(validate.message);
    }
  }
}
