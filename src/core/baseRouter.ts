import express from 'express';
import { Request, Response } from './interfaces';
import { errorService } from './services';
import { BaseController } from './baseController';
import { ON_TRACKING_ERROR } from '../v1/services/crud/errorLog/errorLog.event';
export class BaseRouter<T extends BaseController> {
  router: express.Router = express.Router();
  controller!: T;

  onError(res: Response, error: any) {
    ON_TRACKING_ERROR.next(error);
    if (!error.info) {
      console.log('UNKNOW ERROR', error);
      const err = errorService.somethingWentWrong();
      res.status(err.info.status).json(err.info);
    } else {
      res.status(error.info.status).json(error.info);
    }
  }

  route(func: (req: Request, rep: Response) => Promise<any>) {
    return (req: Request, res: Response) =>
      func
        .bind(this.controller)(req, res)
        .catch((error: any) => {
          this.onError(res, error);
        });
  }
}
