import Ajv from 'ajv';
import AjvError from 'ajv-errors';
import AjvKeyWords from 'ajv-keywords';
import { BaseService } from '../baseService';
import _ from 'lodash';
import crypto from 'crypto';

export class UtilService extends BaseService {
  validateJSON(schema: any, json: any = {}) {
    const ajv = new Ajv({ allErrors: true, jsonPointers: true });
    // @ts-ignore
    AjvError(ajv, { singleError: true });
    AjvKeyWords(ajv, ['switch']);
    const valid = ajv.validate(schema, json);
    return {
      isValid: valid,
      message: ajv.errorsText(),
    };
  }
  booleanString(condition: any) {
    return condition ? 'True' : 'False';
  }
  hash(data: any) {
    let s = data;
    if (!_.isString(data)) {
      s = JSON.stringify(data);
    }
    return crypto.createHash('md5').update(s).digest('hex');
  }
  async keyByModel<T>(model: any, ids: string[], key: string, select: string = '') {
    const query = model.find({
      [key]: { $in: ids },
    });
    if (select != '$all') {
      query.select(`_id ${key} ${select}`);
    }
    return _.keyBy(await query.exec(), key) as { [x: string]: T };
  }
}
