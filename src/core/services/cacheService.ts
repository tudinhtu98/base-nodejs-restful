import { BaseService } from "../baseService";
import _ from "lodash";
import NodeCache from "node-cache";
export type Bucket = {
  bucketName: string;
  cache: NodeCache;
}
export class CacheService extends BaseService {
  buckets: Bucket[] = [];

  constructor() {
    super();
  }

  getBucket(bucketName: string, option: any = {}) {
    let bucket = this.buckets.find(b => b.bucketName == bucketName);
    if (!bucket) {
      const cache = new NodeCache(
        _.merge({ stdTTL: 300, checkperiod: 120, useClones: false }, option)
      );
      bucket = { bucketName, cache };
      this.buckets.push(bucket);
    }
    return bucket;
  }

  set(key: string, value: any, bucketName: string = "default") {
    const bucket = this.getBucket(bucketName);
    bucket.cache.set(key, value);
  }

  get<T>(key: string, bucketName: string = "default") {
    const bucket = this.getBucket(bucketName);
    return bucket.cache.get(key) as T;
  }

  clear(bucketName: string) {
    const bucket = this.getBucket(bucketName);
    bucket.cache.flushAll();
  }
}
