import express from 'express';
import { ON_TRACKING_ERROR } from '../../v1/services/crud/errorLog/errorLog.event';

export interface IErrorInfo {
  status: number;
  code: string;
  message: string;
  data?: any;
}

export class BaseError extends Error {
  constructor(status: number, code: string, message: string, data?: any) {
    super(message);
    this.info = { status, code, message: data ? message + ': ' + data : message };
  }
  info: IErrorInfo;
}
export class ErrorService {
  handleError(func: (req: express.Request, rep: express.Response) => Promise<any>) {
    return (req: express.Request, res: express.Response) =>
      func
        .bind(this)(req, res)
        .catch((error: any) => {
          console.log('ERROR HERE');
          ON_TRACKING_ERROR.next(error);
          if (!error.info) {
            console.log('UNKNOW ERROR', error);
            const err = this.somethingWentWrong();
            res.status(err.info.status).json(err.info);
          } else {
            res.status(error.info.status).json(error.info);
          }
        });
  }
  logError(prefix: string, logOption = true) {
    return (error: any) => {
      ON_TRACKING_ERROR.next(error);
      console.log(prefix, error.message || error, logOption ? error.options : '');
      // try {
      //     sentryService.raven.captureException(error);
      // } catch (err) {
      //     console.log('cannot send to sentry');
      // }
    };
  }
  // Unknow
  somethingWentWrong(message?: string) {
    return new BaseError(500, '500', message || 'Có lỗi xảy ra');
  }
  // Auth
  unauthorized() {
    return new BaseError(401, '401', 'Chưa xác thực');
  }
  badToken() {
    return new BaseError(401, '-1', 'Không có quyền truy cập');
  }
  tokenExpired() {
    return new BaseError(401, '-2', 'Mã truy cập đã hết hạn');
  }
  permissionDeny() {
    return new BaseError(405, '-3', 'Không đủ quyền để truy cập');
  }
  // Request
  requestDataInvalid(message: string) {
    return new BaseError(403, '-4', 'Dữ liệu gửi lên không hợp lệ', message);
  }
  // External Request
  externalRequestFailed(message: string) {
    return new BaseError(500, '-5', 'Có lỗi xảy ra', message);
  }
  // Mongo
  mgRecoredNotFound(message: string = '') {
    return new BaseError(404, '-7', 'Không tìm thấy dữ liệu yêu cầu. ' + message);
  }
  mgQueryFailed(message: string) {
    return new BaseError(403, '-8', message || 'Truy vấn không thành công');
  }
  commentNotExist() {
    return new BaseError(403, '-9', 'ID này không tồn tại trong comment');
  }
  issueDocterCommented(message: string) {
    return new BaseError(403, '-10', message);
  }
  // requestDataInvalid2(message: string) {
  //   return new BaseError(403, '-11', `Dữ liệu gửi lên không hợp lệ: ${message}`);
  // }
}
