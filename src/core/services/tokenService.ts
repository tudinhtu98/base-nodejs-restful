import moment from 'moment';
import jwt from 'jwt-simple';
import { config } from '../../config';
import { errorService } from '.';
export interface IGenerateTokenOption {
  exp?: moment.Moment;
  secret?: string;
}
export interface IDecodeTokenOption {
  secret?: string;
}
export class TokenService {
  constructor() {}

  generateToken(
    payload: any,
    role: string,
    option: IGenerateTokenOption = {}
  ): string {
    const secret = option.secret || config.secret;
    return jwt.encode(
      {
        payload: payload,
        role: role,
        exp: option.exp || moment().add(30, 'days')
      },
      secret
    );
  }
  decodeToken(token: string, option: IDecodeTokenOption = {}) {
    let result = undefined;
    try {
      const secret = option.secret || config.secret;
      result = jwt.decode(token, secret);
    } catch (err) {
      throw errorService.badToken();
    }
    if (result) {
      if (new Date(result.exp).getTime() <= Date.now()) {
        throw errorService.tokenExpired();
      }
      return result;
    } else {
      throw errorService.badToken();
    }
  }
}
