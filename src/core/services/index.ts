import { UtilService } from './utilService';
import { ErrorService } from './errorService';
import { TokenService } from './tokenService';
import { CacheService } from './cacheService';

const errorService = new ErrorService();
const utilService = new UtilService();
const tokenService = new TokenService();
const cacheService = new CacheService();

export {
    errorService,
    utilService,
    tokenService,
    cacheService,
}