import { AuthInfoMiddleware } from './authMiddleware';
import { QueryMiddleware } from "./queryMiddleware";

const authMiddleware = new AuthInfoMiddleware();
const queryMiddleware = new QueryMiddleware();

export { 
    authMiddleware, 
    queryMiddleware,
}