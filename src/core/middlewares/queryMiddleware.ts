import { Request, Response, NextFunction } from '../interfaces';
import { BaseMiddleware } from '../baseMiddleware';
import { config } from '../../config';
import _ from 'lodash';
export class QueryMiddleware extends BaseMiddleware {
  async use(req: Request, res: Response, next: NextFunction, option: any) {
    const filter = this._parseFilter(req, option);
    const order = this._parseOrder(req);
    // @ts-ignore
    const page = parseInt(req.query['page'] || 1);
    // @ts-ignore
    const limit = parseInt(req.query['limit'] || config.pageSize);
    // @ts-ignore
    const offset = parseInt(req.query['offset']) || (page - 1) * limit;
    const fields = this._parseMongoFields(req);
    const populates = this._parsePopulates(req);
    const language = req.query['lang'] || 'vi';
    const textSearch = req.query['textSearch'];

    req.queryInfo = _.merge({
      filter,
      limit,
      offset,
      fields,
      populates,
      order,
      language,
      textSearch,
    });

    next();
  }

  /**
   * Filter param only accept <and> query. <or> will be supported later
   * Format: [[key, operator, value], [key, operator, value]]
   */
  _parseFilter(req: Request, option?: any): any {
    let filter = req.query['filter'];
    try {
      // @ts-ignore
      filter = JSON.parse(filter);
    } catch (ignore) {
      filter = undefined;
    }
    if (option && option.useBody) {
      filter = req.body['filter'];
    }
    return filter || {};
  }

  _parseGroup(req: Request): any {
    let group = req.query['group'];
    try {
      // @ts-ignore
      group = JSON.parse(group);
    } catch (err) {
      group = undefined;
    }
    return group;
  }
  /**
   * Format: [[key, order], [key, order]]
   */
  _parseOrder(req: Request): any {
    let order = req.query['order'];
    try {
      // @ts-ignore
      order = JSON.parse(order);
    } catch (ignore) {
      order = undefined;
    }
    return order || [];
  }
  _parseMongoFields(req: Request) {
    let fields = req.query['fields'];
    try {
      // @ts-ignore
      fields = JSON.parse(fields);
    } catch (ignore) {
      fields = undefined;
    }
    return fields;
  }
  _parsePopulates(req: Request) {
    let populates = req.query['populates'];
    try {
      // @ts-ignore
      populates = JSON.parse(populates);
    } catch (ignore) {
      populates = undefined;
    }
    return populates;
  }
}
