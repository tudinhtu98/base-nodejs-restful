import { userService } from '../../v1/services/crud/user';
import { tokenService } from '../../v1/services/token';
import { BaseMiddleware } from '../baseMiddleware';
import { NextFunction, Request, Response } from '../interfaces';
import { errorService } from '../services';

export class AuthInfoMiddleware extends BaseMiddleware {
  async use(req: Request, res: Response, next: NextFunction, providers: string[] = []) {
    if (providers.includes('block')) {
      throw errorService.permissionDeny();
    }
    if (req.headers['x-token'] && req.headers['x-token']!.toString().split('|').length === 3) {
      const [type, id, token] = req.headers['x-token']!.toString().split('|');
      req.authInfo = { id };
      const decodeToken = tokenService.decodeToken(token);
      switch (type) {
        case 'U':
          // const user = await userService.model
          //   .findOne({ uid: decodeToken.payload.uid })
          //   .exec();
          // if (decodeToken.payload.sessionID != user!.sessionID) {
          //   throw errorService.tokenExpired()
          // }
          // @ts-ignore
          req.authInfo.user = new userService.model(decodeToken.payload).toJSON();
          req.authInfo.uid = decodeToken.payload.uid;
          req.authInfo.provider = decodeToken.role;
          await this.permissionVerify(req, providers);
          break;
        default:
          throw errorService.badToken();
      }
      next();
      return;
    }
    if (providers.includes('*')) return next();
    throw errorService.unauthorized();
  }
  async permissionVerify(req: Request, providers: string[], option = {}) {
    if (providers.includes('*')) return true;
    for (const p of providers) {
      if (req.authInfo!.provider!.includes(p)) {
        return true;
      }
    }
    throw errorService.permissionDeny();
  }
}
