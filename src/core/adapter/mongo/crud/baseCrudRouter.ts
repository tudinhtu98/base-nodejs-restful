import { BaseCrudController } from './baseCrudController';
import { BaseRouter } from '../../../baseRouter';
import { authMiddleware, queryMiddleware } from '../../../middlewares';
export class BaseCrudRouter<T extends BaseCrudController<any>> extends BaseRouter<T> {
    constructor(controller: T) {
        super()
        this.controller = controller;
        this.customRouting();
        this.defaultRouting();
    }
    controller: T;
    defaultRouting() {
        this.router.get('/count', this.countMiddlewares(), this.route(this.controller.count));
        this.router.get('/', this.getListMiddlewares(), this.route(this.controller.getList));
        this.router.get('/:_id', this.getItemMiddlewares(), this.route(this.controller.getItem))
        this.router.post('/', this.createMiddlewares(), this.route(this.controller.create))
        this.router.put('/:_id', this.updateMiddlewares(), this.route(this.controller.update))
        this.router.delete('/:_id', this.deleteMiddlewares(), this.route(this.controller.delete))
        this.router.delete('/', this.deleteAllMiddlewares(), this.route(this.controller.deleteAll))
        
    }
    customRouting() {

    }
    countMiddlewares(): any[] {
        return [
            authMiddleware.run(["admin", "write", "read"]),
            queryMiddleware.run()
        ]
    }
    getListMiddlewares(): any[] {
        return [
            authMiddleware.run(["admin", "write", "read"]),
            queryMiddleware.run()
        ]
    }
    getItemMiddlewares(): any[] {
        return [
            authMiddleware.run(["admin", "write", "read"]),
            queryMiddleware.run()
        ]
    }
    createMiddlewares(): any[] {
        return [authMiddleware.run([])]
    }
    updateMiddlewares(): any[] {
        return [authMiddleware.run(["admin", "write"])]
    }
    deleteMiddlewares(): any[] {
        return [authMiddleware.run(["admin", "write"])]
    }
    deleteAllMiddlewares(): any[] {
        return [authMiddleware.run(["admin", "write"])]
    }
}
