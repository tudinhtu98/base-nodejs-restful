import _ from 'lodash';

import { config } from '../../../../config';
import { utilService } from '../../../../v1/services';
import { BaseService } from '../../../baseService';
import { ICrudExecOption, ICrudOption } from '../../../interfaces';
import { cacheService, errorService } from '../../../services';
import { Bucket } from '../../../services/cacheService';
import { BaseDocument, DocumentQuery, Model } from '../baseModel';

export type CrudServiceOption = {
  cache?: {
    stdTTL?: number;
    checkperiod?: number;
    useClones?: boolean;
  };
};
export class BaseCrudService<T extends BaseDocument> extends BaseService {
  constructor(model: Model<T>, option: CrudServiceOption = {}) {
    super();
    this.model = model;
    this.bucket = cacheService.getBucket(Math.random().toString().substr(3, 15), option.cache);
  }
  model: Model<T>;
  bucket: Bucket;

  async exec<Y>(promise: any, option: ICrudExecOption = { allowNull: true }): Promise<Y> {
    try {
      const result = await promise;
      if ((result === undefined || result === null) && !option.allowNull)
        throw errorService.mgRecoredNotFound();
      return result;
    } catch (err) {
      if (err.info) throw err;
      if (config.debug) {
        console.log('err', err);
        if (err.errors && err.errors[0]) {
          throw errorService.mgQueryFailed(err.errors[0].message);
        } else {
          throw errorService.mgQueryFailed(err.message);
        }
      } else throw err;
    }
  }
  async count(option: ICrudOption = {}): Promise<number> {
    let query = this.model.countDocuments({});
    query = this.applyQueryOptions(query, option);
    return await this.exec<number>(query);
  }
  async getList(option: ICrudOption = {}) {
    let hash = '';
    if (option.cached) {
      hash = `list-${utilService.hash(option)}`;
      const result = cacheService.get<{ count: number; rows: T[] }>(hash, this.bucket.bucketName);
      if (result) return result;
    }
    let query = this.model.find();
    query = this.applyQueryOptions(query, option);
    query.setOptions({
      // @ts-ignore
      toJson: { virtual: true },
    });
    if (option.limit && option.limit > 0) query.limit(option.limit);
    if (option.offset) query.skip(option.offset);
    const [rows, count] = await Promise.all([this.exec<T[]>(query), this.count(option)]);
    let data: T[] = rows;
    if (option.language != 'vi') {
      data = [];
      for (let i = 0; i < rows.length; i++)
        data.push(_.merge(rows[i].toJSON(), _.get(rows[i], `lang[${option.language}]`)));
    }
    const result = { count, rows: data };

    if (option.cached) cacheService.set(hash, { count, rows: data }, this.bucket.bucketName);
    return result;
  }
  async getItem(option: ICrudOption = {}) {
    let hash = '';
    if (option.cached) {
      hash = `item-${utilService.hash(option)}`;
      const result = cacheService.get<T>(hash, this.bucket.bucketName);
      if (result) return result;
    }
    let query = this.model.findOne();
    query = this.applyQueryOptions(query, option);
    if (option.offset) query.skip(option.offset);
    const result = await this.exec<T>(query, { allowNull: option.allowNull });

    let data = result.toJSON();
    if (option.language != 'vi') {
      data = _.merge(result.toJSON(), _.get(result.toJSON(), `lang[${option.language}]`));
    }
    if (option.cached) cacheService.set(hash, data, this.bucket.bucketName);
    return data;
  }
  async create(params: T, option: ICrudOption = {}) {
    const query = this.model.create(params);
    this.bucket.cache.flushAll();
    return await this.exec<T>(query);
  }
  async update(params: T, option: ICrudOption = {}) {
    // updateLang
    if (params.lang != 'vi') {
      const data: any = await this.model.findOne(option.filter);
      const lang = this.getDefault(data.lang, {});
      for (const value in params.lang) {
        lang[value] = Object.assign({}, lang[value], params.lang[value]);
      }
      params.lang = lang;
    }
    // @ts-ignore
    const query = this.model.findOneAndUpdate(option.filter, params, {
      new: true,
    });
    this.bucket.cache.flushAll();
    return await this.exec<T>(query);
  }
  async delete(option: ICrudOption = {}) {
    let query = this.model.findOne();
    query = this.applyQueryOptions(query, option);
    const item = await this.exec<T>(query, { allowNull: false });
    this.bucket.cache.flushAll();
    return this.exec<T>(item.remove());
  }
  async deleteAll(option: ICrudOption = {}) {
    let query = this.model.remove(option.filter);
    query = this.applyQueryOptions(query, option);
    this.bucket.cache.flushAll();
    return await this.exec<{ n: number; ok: number; deletedCount: number }>(query);
  }
  applyQueryOptions(query: DocumentQuery, option: ICrudOption) {
    if (option.textSearch) {
      if (option.textSearch.includes(' ')) {
        _.set(option, 'filter.$text.$search', option.textSearch);
        query.select({ _score: { $meta: 'textScore' } });
        query.sort({ _score: { $meta: 'textScore' } });
        // _.set(option, 'order._score', { $meta: 'textScore' });
      } else {
        const textSearchIndex = this.model.schema
          .indexes()
          .find((c) => Object.values(c[0]!).some((d) => d == 'text'));
        if (textSearchIndex) {
          const or: any[] = [];
          Object.keys(textSearchIndex[0]!).forEach((key) => {
            or.push({
              [key]: {
                $regex: option.textSearch,
                $options: 'i',
              },
            });
          });
          _.set(option, `filter.$or`, or);
        }
      }
    }
    if (option.filter) query.where(option.filter);
    if (option.fields) query.select(option.fields);
    if (option.populates) {
      for (const populate of option.populates) {
        query.populate(populate);
      }
    }
    if (option.lean) query.lean();
    if (option.order) query.sort(option.order);
    return query;
  }
  getDefault = (v: any, d: any) => {
    return v || d;
  };
}
