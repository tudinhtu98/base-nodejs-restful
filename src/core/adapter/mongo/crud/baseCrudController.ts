import { BaseController } from '../../../baseController';
import { BaseCrudService } from './baseCrudService';
import { Request, Response, IValidateSchema } from '../../../interfaces';
import _ from 'lodash';

export class BaseCrudController<T extends BaseCrudService<any>> extends BaseController {
  constructor(service: T) {
    super();
    this.service = service;
  }
  service: T;
  async dependFilter(req: Request) {
    return {};
  }
  async appendCreateData(req: Request) {
    return {};
  }
  async appendUpdateData(req: Request) {
    return {};
  }
  createSchema: IValidateSchema = { type: 'object' };
  updateSchema: IValidateSchema = { type: 'object' };
  async count(req: Request, res: Response) {
    const result = (await this.service.count(req.queryInfo)) || 0;
    this.onSuccess(res, result);
  }
  async getList(req: Request, res: Response) {
    _.set(
      req.queryInfo!,
      'filter',
      _.merge(_.get(req.queryInfo, 'filter'), await this.dependFilter(req))
    );
    const { count, rows } = await this.service.getList(req.queryInfo);
    this.onSuccessAsList(res, rows, count, req.queryInfo);
  }
  async getItem(req: Request, res: Response) {
    _.set(
      req.queryInfo!,
      'filter',
      _.merge(_.get(req.queryInfo, 'filter'), await this.dependFilter(req), {
        _id: req.params['_id'],
      })
    );
    const result = await this.service.getItem(req.queryInfo);
    this.onSuccess(res, result);
  }
  async create(req: Request, res: Response) {
    await this.validateJSON(req.body, this.createSchema);
    const result = await this.service.create(_.merge(req.body, await this.appendCreateData(req)));
    this.onSuccess(res, result);
  }
  async update(req: Request, res: Response) {
    await this.validateJSON(req.body, this.updateSchema);
    const result = await this.service.update(_.merge(req.body, await this.appendUpdateData(req)), {
      filter: _.merge({ _id: req.params._id }, await this.dependFilter(req)),
    });
    this.onSuccess(res, result);
  }
  async delete(req: Request, res: Response) {
    const { _id } = req.params;
    const result = await this.service.delete({
      filter: _.merge({ _id }, await this.dependFilter(req)),
    });
    this.onSuccess(res, result);
  }
  async deleteAll(req: Request, res: Response) {
    if (_.has(req.query, 'items')) {
      req.query.items = JSON.parse(req.query.items as any) || [];
    }
    await this.validateJSON(req.query, {
      type: 'object',
      properties: {
        items: {
          type: 'array',
          uniqueItems: true,
          minItems: 1,
          items: { type: 'string' },
        },
      },
      required: ['items'],
      additionalProperties: false,
    });
    const { items } = req.query;
    const result = await this.service.deleteAll({
      filter: _.merge({ _id: { $in: items } }, await this.dependFilter(req)),
    });
    this.onSuccess(res, result);
  }
}
