import mongoose from 'mongoose';
export type BaseDocument = mongoose.Document & {
  createdAt?: Date;
  status?: string;
  updatedAt?: Date;
  lang?: any;
};

export type Model<T extends BaseDocument> = mongoose.Model<T>;
export type DocumentQuery = mongoose.DocumentQuery<any, any>;
