import mongoose from 'mongoose';
import mongooseHidden from 'mongoose-hidden';

import { BaseDocument } from '@core/adapter/mongo/baseModel';
import { MainConnection } from './connections/main';

const Schema = mongoose.Schema;

export type UserDocument = BaseDocument & {
  uid: string;
  fullName: string;
  phone: string;
  email: string;
  avatar: string;
  address: string;
  type: string;
};

const userSchema = new Schema(
  {
    uid: { type: String, required: true, unique: true },
    fullName: { type: String },
    phone: { type: String },
    email: { type: String },
    avatar: { type: String },
    address: { type: String },
    type: { type: String, enum: ['editor', 'doctor', 'member'] },
    status: { type: String, enum: ['active', 'deactive'], default: 'active' },
  },
  { timestamps: true }
);

userSchema.index({
  fullName: 'text',
  phone: 'text',
  email: 'text',
  address: 'text',
});
userSchema.plugin(mongooseHidden({ defaultHidden: { _id: false } }));

export const UserModel = MainConnection.model<UserDocument>('User', userSchema);
