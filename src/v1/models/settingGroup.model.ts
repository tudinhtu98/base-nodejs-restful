import { BaseDocument } from '@core/adapter/mongo/baseModel';
import mongoose from 'mongoose';
import { MainConnection } from './connections/main';

const Schema = mongoose.Schema;

export type SettingGroupDocument = BaseDocument & {
  slug: string;
  name: string;
  desc: string;
  readOnly: boolean;
  settingIds: string[];
};

const settingGroupSchema = new Schema(
  {
    slug: { type: String, required: true },
    name: { type: String, required: true },
    desc: { type: String },
    readOnly: { type: Boolean, default: false },
    settingIds: { type: [{ type: Schema.Types.ObjectId, ref: 'Setting' }], default: [] },

    status: { type: String, enum: ['active', 'deactive'], default: 'active' },
  },
  { timestamps: true }
);

export const SettingGroupModel = MainConnection.model<SettingGroupDocument>(
  'SettingGroup',
  settingGroupSchema
);
