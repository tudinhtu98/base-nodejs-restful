import mongoose from 'mongoose';
import { config } from '@config';

const connect = mongoose.createConnection(config.mgConnection, {
  readPreference: 'secondaryPreferred',
  useNewUrlParser: true,
  socketTimeoutMS: 30000,
  keepAlive: true,
  useFindAndModify: false,
  useCreateIndex: true,
  useUnifiedTopology: true,
});

export const MainConnection = connect;
