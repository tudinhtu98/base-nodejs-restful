import { BaseDocument } from '@core/adapter/mongo/baseModel';
import mongoose from 'mongoose';
import { MainConnection } from './connections/main';

const Schema = mongoose.Schema;

export type SettingDocument = BaseDocument & {
  name: string;
  key: string;
  value: any;
  type: string;
  isActive: boolean;
  isPrivate: boolean;
  readOnly: boolean;
  groupId: string;
  lang: any;
};

const settingSchema = new Schema(
  {
    name: { type: String, required: true },
    key: { type: String, required: true },
    value: { type: Schema.Types.Mixed, required: true },
    type: { type: String, required: true, default: 'string', enum: ['string', 'number', 'object'] },
    isActive: { type: Boolean, required: true, default: true },
    isPrivate: { type: Boolean, required: true, default: false },
    readOnly: { type: Boolean, default: false },
    groupId: { type: Schema.Types.ObjectId, ref: 'SettingGroup', required: true },
    lang: { type: Schema.Types.Mixed },

    status: { type: String, enum: ['active', 'deactive'], default: 'active' },
  },
  { timestamps: true }
);

export const SettingModel = MainConnection.model<SettingDocument>('Setting', settingSchema);
