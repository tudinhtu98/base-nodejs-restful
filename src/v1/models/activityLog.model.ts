import { BaseDocument } from '@core/adapter/mongo/baseModel';
import mongoose from 'mongoose';
import { MainConnection } from './connections/main';

const Schema = mongoose.Schema;

export type ActivityLogDocument = BaseDocument & {
  user: string;
  timestamp: Date;
  message: string;
};

const activityLogSchema = new Schema(
  {
    user: { type: Schema.Types.ObjectId, required: true },
    timestamp: { type: Date, required: true },
    message: { type: String, required: true },

    status: { type: String, enum: ['active', 'deactive'], default: 'active' },
  },
  { timestamps: true }
);

export const ActivityLogModel = MainConnection.model<ActivityLogDocument>(
  'ActivityLog',
  activityLogSchema
);
