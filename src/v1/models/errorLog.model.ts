import { BaseDocument } from '@core/adapter/mongo/baseModel';
import mongoose from 'mongoose';
import { MainConnection } from './connections/main';

const Schema = mongoose.Schema;

export type ErrorLogDocument = BaseDocument & {
  hash: string;
  message: string;
  name: string;
  stack: string[];
  count: number;
  date: Date;
};

const errorLogSchema = new Schema(
  {
    hash: { type: String, required: true },
    message: { type: String, required: true },
    name: { type: String, required: true },
    stack: { type: [String], required: true },
    count: { type: Number, default: 1 },
    date: { type: Date },

    status: { type: String, enum: ['active', 'deactive'], default: 'active' },
  },
  { timestamps: true }
);

errorLogSchema.index({ hash: 1 }, { unique: true });

export const ErrorLogModel = MainConnection.model<ErrorLogDocument>('ErrorLog', errorLogSchema);
