import { EtagMiddleware } from "./etag.middleware";

const etagMiddleware = new EtagMiddleware();

export { etagMiddleware, EtagMiddleware }