import { BaseMiddleware } from '@core/baseMiddleware';
import { NextFunction, Request, Response } from '@core/interfaces';
import { Types } from 'mongoose';
import { cacheService } from '@core/services';
export class EtagMiddleware extends BaseMiddleware {
  async use(req: Request, res: Response, next: NextFunction) {
    let etag = req.header('If-None-Match');
    if (!etag || !Types.ObjectId.isValid(etag!.split('.')[2] as string)) {
      etag = `etag.primary.${Types.ObjectId().toHexString()}`;
    }
    const cachedEtag = cacheService.get<string>(etag);
    if (!cachedEtag) {
      cacheService.set(etag, `etag.secondary.${Types.ObjectId().toHexString()}`);
      req.etag = etag.split('.')[2];
    } else {
      cacheService.clear(etag);
      etag = cachedEtag;
      if (cachedEtag.split('.')[1] == 'primary') {
        req.etag = cachedEtag.split('.')[2];
        cacheService.set(etag, `etag.secondary.${req.etag}`);
      } else {
        req.etag = etag.split('.')[2];
        cacheService.set(etag, `etag.primary.${req.etag}`);
      }
    }
    res.setHeader('ETAG', etag);
    next();
  }
}
