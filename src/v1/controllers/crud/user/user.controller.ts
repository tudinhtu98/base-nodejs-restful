import _ from 'lodash';
import { ROLES } from '@config/role';
import { BaseCrudController } from '@core/adapter/mongo/crud/baseCrudController';
import { Request, Response } from '@core/interfaces';
import { UserDocument } from '@models/user.model';
import { errorService } from '@services';
import { UserService, userService } from '@services/crud/user';
import { firebaseService } from '@services/firebase';
import { tokenService } from '@services/token';
import { UserSchema } from './user.schema';
import { ON_TRACKING_ACTIVITY } from '@services/crud/activityLog/activityLog.event';

export class UserController extends BaseCrudController<UserService> {
  constructor() {
    super(userService);
  }
  async dependFilter(req: Request) {
    if (
      req.authInfo!.provider == ROLES.MEMBER &&
      req.params['_id'] != req.authInfo!.user!._id.toString()
    ) {
      throw errorService.permissionDeny();
    }
    return {};
  }
  async create(req: Request, res: Response) {
    await this.validateJSON(req.body, UserSchema.create);
    if (_.trim(req.body.fullName) == '') {
      throw errorService.requestDataInvalid('Họ tên không được rỗng hoặc chỉ có số');
    }
    // Create firebase user
    const { email, password } = req.body;
    const firebaseUser = await firebaseService.createUser({ email, password }).catch((err) => {
      throw errorService.createUserErorr(err.message);
    });
    req.body.fullName = _.trim(req.body.fullName);
    const user = { ...req.body, uid: firebaseUser.uid };
    delete user.password;
    const result = await this.service.create(user);
    this.onSuccess(res, result);
  }
  async update(req: Request, res: Response) {
    await this.validateJSON(req.body, UserSchema.update);
    if (req.body.fullName && _.trim(req.body.fullName) == '') {
      throw errorService.requestDataInvalid('Họ tên không được rỗng hoặc chỉ có số');
    }
    req.body.fullName = _.trim(req.body.fullName);
    const result = await this.service.update(req.body, {
      filter: _.merge({ _id: req.params._id }, await this.dependFilter(req)),
    });
    this.onSuccess(res, result);
  }
  async delete(req: Request, res: Response) {
    const { _id } = req.params;
    const result = await this.service.delete({
      filter: { _id: _id },
    });
    firebaseService
      .removeUser(result.get('uid'))
      .catch(errorService.logError(`Error when remove firebase User ${result.uid}`));
    this.onSuccess(res, result);
  }
  async deleteAll(req: Request, res: Response) {
    if (_.has(req.query, 'items')) {
      req.query.items = JSON.parse(req.query.items as any) || [];
    }
    await this.validateJSON(req.query, {
      type: 'object',
      properties: {
        items: { type: 'array', uniqueItems: true, minItems: 1, items: { type: 'string' } },
      },
      required: ['items'],
      additionalProperties: false,
    });
    const { items } = req.query;

    let records = await this.service.model.find({
      _id: {
        $in: items,
      },
    });

    for (let i in records) {
      if (records[i].get('uid')) await firebaseService.removeUser(records[i].get('uid'));
      await records[i].remove();
    }

    // const { rows: users } = await this.service.getList({
    //   filter: _.merge({ _id: { $in: items } }, await this.dependFilter(req)),
    // });
    // await this.service.deleteAll({
    //   filter: _.merge({ _id: { $in: items } }, await this.dependFilter(req)),
    // });
    // if (users) {
    //   users.forEach((u) => {
    //     firebaseService
    //       .removeUser(u.get('uid'))
    //       .catch(errorService.logError(`Error when remove firebase User ${u.uid}`));
    //   });
    // }

    this.onSuccess(res, records);
  }
  async login(req: Request, res: Response) {
    await this.validateJSON(req.body, UserSchema.login);
    const token = req.body.token.split('|')[2];
    const firebaseUser = await firebaseService.verifyIdToken(token);
    let user = await this.service.model.findOne({ uid: firebaseUser.uid }).exec();
    // if (user) {
    //   await userDeviceService.model.deleteMany({ sessionID: user.sessionID });
    // }
    switch (firebaseUser.firebase.sign_in_provider) {
      case 'password':
        if (!user) throw errorService.userNotExist();
        break;
      case 'phone':
        if (!user) {
          user = await this.service.create({
            uid: firebaseUser.uid,
            fullName: '', // firebaseUser.phone_number,
            email: '',
            phone: firebaseUser.phone_number,
            type: ROLES.MEMBER,
          } as UserDocument);
        }
        break;
      default:
        throw errorService.userRoleNotSupported();
    }
    ON_TRACKING_ACTIVITY.next({ user: user._id, message: `${user.fullName} đã đăng nhập` });
    return this.onSuccess(res, {
      token: tokenService.createUserToken(user!),
      user,
    });
  }
}
