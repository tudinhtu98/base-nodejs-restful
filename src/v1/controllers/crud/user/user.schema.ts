import { IValidateSchema } from '../../../../core/interfaces';

export class UserSchema {
  static create: IValidateSchema = {
    type: 'object',
    properties: {
      fullName: { type: 'string' },
      email: { type: 'string' },
      phone: { type: 'string' },
      address: { type: 'string' },
      password: { type: 'string' },
      type: { type: 'string' },
    },
    required: ['fullName', 'phone', 'type'],
  };
  static update: IValidateSchema = {
    type: 'object',
    properties: {
      fullName: { type: 'string' },
      email: { type: 'string' },
      address: { type: 'string' },
      avatar: { type: 'string' },
    },
  };
  static login: IValidateSchema = {
    type: 'object',
    properties: {
      token: { type: 'string' },
    },
    required: ['token'],
  };
  static changePassword: IValidateSchema = {
    type: 'object',
    properties: {
      password: { type: 'string' },
    },
    required: ['password'],
  };
}
