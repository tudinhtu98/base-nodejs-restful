import { BaseCrudController } from "../../../../core/adapter/mongo/crud/baseCrudController";
import { ActivityLogService, activityLogService } from "../../../services/crud/activityLog";
import { Request } from "../../../../core/interfaces";

export class ActivityLogController extends BaseCrudController<ActivityLogService> {
    constructor() {
        super(activityLogService);
    }
    async appendCreateData(req: Request) {
        return { };
    }
    async appendUpdateData(req: Request) {
        return { };
    }
}