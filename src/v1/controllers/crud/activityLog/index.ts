import { ActivityLogController } from './activityLog.controller';

const activityLogController = new ActivityLogController();

export { ActivityLogController, activityLogController }