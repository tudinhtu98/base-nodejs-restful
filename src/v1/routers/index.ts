import  fs from 'fs'
import  path from 'path'
import  express from 'express'
import _ from 'lodash'

function regisRouter(route: express.Router, node: string, dir: string) {
    const modules = fs.readdirSync(dir);
    const subRoute = express.Router();
    modules.forEach(module => {
        const matchs = /(.*).router.js/g.exec(module);
        if (!matchs || matchs.length < 1 || _.endsWith(module, '.map') || _.startsWith(module, 'index') || fs.lstatSync(path.join(dir, module)).isDirectory()) return;
        const { default: Router } = require(path.join(dir, module))
        const router = new Router()
        module = module.split('.')[0]
        subRoute.use(`/${module}`, router.router)
    })
    route.use('/' + node, subRoute);
    const subNodes = fs.readdirSync(dir);
    subNodes.forEach(subNode => {
        const subNodeDir = path.join(dir, subNode);
        if (fs.lstatSync(subNodeDir).isDirectory()) {
            const subNodeRoute = regisRouter(route, subNode, subNodeDir);
            route.use('/' + subNode, subNodeRoute);
        }
    })
    return route
}
const regisRoute = regisRouter(express.Router(), '', __dirname);
export default regisRoute;