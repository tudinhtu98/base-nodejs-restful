import { ROLES } from '@config/role';
import { BaseCrudRouter } from '@core/adapter/mongo/crud/baseCrudRouter';
import { authMiddleware, queryMiddleware } from '@core/middlewares';
import { UserController, userController } from '@controllers/crud/user';

export default class UserRouter extends BaseCrudRouter<UserController> {
  constructor() {
    super(userController);
  }
  customRouting() {
    this.router.post('/login', this.route(this.controller.login));
    this.router.post('/loginEmail', this.route(this.controller.login));
  }

  countMiddlewares(): any[] {
    return [authMiddleware.run([ROLES.EDITOR]), queryMiddleware.run()];
  }
  getListMiddlewares(): any[] {
    return [authMiddleware.run([ROLES.EDITOR]), queryMiddleware.run()];
  }
  getItemMiddlewares(): any[] {
    return [authMiddleware.run([ROLES.EDITOR, ROLES.DOCTOR, ROLES.MEMBER]), queryMiddleware.run()];
  }
  createMiddlewares(): any[] {
    return [authMiddleware.run([ROLES.EDITOR])];
  }
  updateMiddlewares(): any[] {
    return [authMiddleware.run([ROLES.EDITOR, ROLES.DOCTOR, ROLES.MEMBER])];
  }
  deleteMiddlewares(): any[] {
    return [authMiddleware.run([ROLES.EDITOR])];
  }
  deleteAllMiddlewares(): any[] {
    return [authMiddleware.run([ROLES.EDITOR])];
  }
}
