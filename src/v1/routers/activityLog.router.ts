import { BaseCrudRouter } from '@core/adapter/mongo/crud/baseCrudRouter';
import { activityLogController, ActivityLogController } from '@controllers/crud/activityLog';
import { authMiddleware, queryMiddleware } from '@core/middlewares';

export default class ActivityLogRouter extends BaseCrudRouter<ActivityLogController> {
  constructor() {
    super(activityLogController);
  }
  customRouting() {}

  countMiddlewares(): any[] {
    return [authMiddleware.run([]), queryMiddleware.run()];
  }
  getListMiddlewares(): any[] {
    return [authMiddleware.run([]), queryMiddleware.run()];
  }
  getItemMiddlewares(): any[] {
    return [authMiddleware.run([]), queryMiddleware.run()];
  }
  createMiddlewares(): any[] {
    return [authMiddleware.run([])];
  }
  updateMiddlewares(): any[] {
    return [authMiddleware.run([])];
  }
  deleteMiddlewares(): any[] {
    return [authMiddleware.run([])];
  }
  deleteAllMiddlewares(): any[] {
    return [authMiddleware.run([])];
  }
}
