import { ErrorService as BaseErrorService, BaseError } from '@core/services/errorService';
export class ErrorService extends BaseErrorService {
  userNotExist() {
    return new BaseError(403, '-103', 'Người dùng khồng tồn tại');
  }
  userExisted() {
    return new BaseError(403, '-104', 'Người dùng đã tồn tại');
  }
  userRoleNotSupported() {
    return new BaseError(401, '-105', 'Người dùng không được cấp quyền');
  }
  createUserErorr(message: string) {
    return new BaseError(401, '-106', `Lỗi tạo người dùng: ${message}`);
  }
  cannotCancelSharingOrder(message: string) {
    return new BaseError(401, '-107', `Không thể huỷ đơn hàng: ${message}`);
  }
}
