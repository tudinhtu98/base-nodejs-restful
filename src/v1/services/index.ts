import { ErrorService } from './errorService';
import { UtilService } from './utilService';

const errorService = new ErrorService();
const utilService = new UtilService();
const multilanguage = utilService.multilanguage
export {
    errorService,
    utilService,
    multilanguage,
}