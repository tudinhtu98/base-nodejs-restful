import nodemailer from 'nodemailer';
import { settingService } from '../crud/setting';

export interface IMailOption {
  from?: string; // sender address
  to?: string; // list of receivers
  subject?: string; // Subject line
  text?: string; // plain text body
  html?: string; // html body,
  attachments?: any[];
}

export async function baseSend(mailOption: IMailOption = { attachments: [] }) {
  const [host, port, username, password] = await Promise.all([
    settingService.model.findOne({ key: 'email_host' }),
    settingService.model.findOne({ key: 'email_port' }),
    settingService.model.findOne({ key: 'email_username' }),
    settingService.model.findOne({ key: 'email_password' }),
  ]);

  mailOption.from = `"VẢI THIỀU BẮC GIANG" <${username ? username.value : ''}>`;

  let transporter = nodemailer.createTransport({
    host: host ? host.value : 'smtp.gmail.com',
    port: port ? port.value : 465,
    secure: port ? port.value : 465 === 465 ? true : false, // true for 465, false for other ports
    auth: {
      user: username ? username.value : '', // generated ethereal user
      pass: password ? password.value : '', // generated ethereal password
    },
  });
  return await transporter.sendMail(mailOption);
}
