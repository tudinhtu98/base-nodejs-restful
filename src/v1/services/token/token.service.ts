import { TokenService as BaseTokenService } from '@core/services/tokenService';
import { UserDocument } from '@models/user.model';

export class TokenService extends BaseTokenService {
  constructor() {
    super();
  }
  createUserToken(user: UserDocument) {
    return ['U', user.uid, this.generateToken(user, user.type!)].join(
      '|'
    );
    // => U|123123123|abcxzy
  }
}
