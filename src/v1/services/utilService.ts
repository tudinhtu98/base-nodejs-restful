import { UtilService as BaseUtilService } from '@core/services/utilService';
import _ from 'lodash';
import vm from 'vm2';
import { Request, Response } from '@core/interfaces';
import * as json2csv from 'json2csv';

// import _ from "lodash";
export class UtilService extends BaseUtilService {
  parseObjectWithInfo(params: { object: any; info: any }) {
    const { info, object } = params;
    let encodeData = JSON.stringify(object);
    encodeData = this.parseStringWithInfo({ data: encodeData, info });
    try {
      return JSON.parse(encodeData);
    } catch (err) {
      throw err;
    }
  }
  parseStringWithInfo(params: { data: string; info: any }) {
    const { data, info } = params;
    let messageText = '' + data;
    const stringRegex = /{{(.*?)}}/g;
    messageText = messageText.replace(stringRegex, (m: any, field: string) => {
      let data: string;
      try {
        data = this.execParseStringWithInfoScript({ info, field });
      } catch (error) {
        return '';
      }
      if (_.isString(data) || _.isNumber(data)) {
        data = JSON.stringify(data)
          .replace(/\\n/g, '\\n')
          .replace(/\\'/g, "\\'")
          .replace(/\\"/g, '\\"')
          .replace(/\\&/g, '\\&')
          .replace(/\\r/g, '\\r')
          .replace(/\\t/g, '\\t')
          .replace(/\\b/g, '\\b')
          .replace(/\\f/g, '\\f')
          .replace(/^\"(.*)\"$/g, '$1');
      } else if (_.isObject(data) || _.isBoolean(data)) {
        data = `<<Object(${JSON.stringify(data)})Object>>`;
      }
      return data || '';
    });
    return messageText.replace(
      /\:\"(?: +)?<<Object\((true|false|[\{|\[].*?[\}|\]])\)Object>>(?: +)?\"/g,
      ':$1'
    );
  }
  execParseStringWithInfoScript(params: { info: any; field: string }) {
    const { info, field } = params;
    const options: any = {
      timeout: 200,
      sandbox: _.merge({}, {}, info),
    };
    const result = new vm.VM(options).run(field.replace(/\\(\S)/g, '$1'));
    return result;
  }
  responseCSV(res: Response, data: any, filename = 'csv') {
    res.status(200);
    res.setHeader('Content-Type', 'text/csv');
    res.setHeader(
      'Content-disposition',
      `attachment; filename=${filename.replace(/\ /g, ' -')}.csv`
    );
    res.send('\ufeff' + json2csv.parse(data));
  }
  multilanguage(req: Request, rows: any) {
    let data: any[] = rows;
    const getDefault = (v: any, d: any) => {
      return v || d;
    };
    if (req.queryInfo!.language != 'vi') {
      data = [];
      for (let i = 0; i < rows.length; i++)
        data.push({
          ...rows[i].toJSON(),
          ...getDefault(rows[i].toJSON()[`lang`], {})[`${req.queryInfo!.language}`],
        });
    }
    return data;
  }
  getDefault(v: any, d: any) {
    return v || d;
  }
  parsePhone(phone: string, pre: string) {
    if (!phone) return phone;
    let newPhone = '' + phone;
    newPhone = newPhone.replace(/^\+84/i, pre).replace(/^\+0/i, pre).replace(/^84/i, pre);

    return newPhone;
  }
}
