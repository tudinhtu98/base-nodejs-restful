import admin from 'firebase-admin';
import _ from 'lodash';
import moment from 'moment';

import { errorService } from '..';
import { BaseService } from '../../../core/baseService';

// import { } from './firebase.helper';
const firebaseCredential = require('../../../../firebase-service-account.json');
if (!firebaseCredential) {
  throw new Error('MISSING file firebase-service-account.json');
}
interface ISystemConfig {
  email?: {
    host?: string;
    pass?: string;
    port?: number;
    service?: string;
    user?: string;
  };
}

export class FirebaseService extends BaseService {
  constructor() {
    super();

    this.app = admin.initializeApp(
      {
        credential: admin.credential.cert(firebaseCredential as admin.ServiceAccount),
        databaseURL: `https://${firebaseCredential.project_id}.firebaseio.com`,
        storageBucket: `${firebaseCredential.project_id}.appspot.com`,
      },
      'v1'
    );
    this.app
      .database()
      .ref('system')
      .on('value', (snap) => {
        if (snap) {
          this.systemConfig = snap.val();
          console.log('update system config', this.systemConfig);
        }
      });
  }
  app: admin.app.App;
  systemConfig: ISystemConfig = {};
  getConfig(path?: string) {
    if (path) {
      return _.get(this.systemConfig, path);
    } else {
      return this.systemConfig;
    }
  }
  get messaging() {
    return this.app.messaging();
  }
  async verifyIdToken(token: string) {
    try {
      return await this.app.auth().verifyIdToken(token);
    } catch (err) {
      throw errorService.badToken();
    }
  }
  async createUser(params: { email: string; password: string }) {
    try {
      return await this.app.auth().createUser({
        email: params.email,
        password: params.password,
      });
    } catch (err) {
      throw err;
    }
  }
  async removeUser(uid: string) {
    return await this.app.auth().deleteUser(uid);
  }
  async getUserByEmail(params: { email: string }) {
    try {
      return await this.app.auth().getUserByEmail(params.email);
    } catch (err) {
      throw err;
    }
  }
  async getUserByPhone(params: { phone: string }) {
    try {
      return await this.app.auth().getUserByPhoneNumber(params.phone);
    } catch (err) {
      throw err;
    }
  }
  async getUserById(params: { uid: string }) {
    return await this.app.auth().getUser(params.uid);
  }
  async uploadFile(path: string) {
    const bucket = this.app.storage().bucket();
    try {
      return await bucket.upload(path);
    } catch (error) {
      throw errorService.externalRequestFailed(error.message);
    }
  }
  async uploadBuffer(buffer: any, filename: string) {
    const bucket = this.app.storage().bucket();
    const file = bucket.file(filename);
    // var buff = Buffer.from(buffer, 'binary').toString('utf-8');
    try {
      const stream = file.createWriteStream({
        metadata: {
          contentType: 'application/pdf',
        },
      });
      stream.on('error', (err) => {
        console.log('err', err);
      });
      stream.on('finish', () => {
        console.log(filename);
      });
      stream.end(buffer);
    } catch (error) {
      throw errorService.externalRequestFailed(error.message);
    }
  }
  async getFile(filename: string) {
    const file = this.app.storage().bucket().file(filename);
    try {
      return await file.download().catch(() => {
        console.log(`File ${filename} không tồn tại.`);
      });
    } catch (error) {
      throw errorService.externalRequestFailed(error.message);
    }
  }
  async getFileURL(filename: string) {
    const file = this.app.storage().bucket().file(filename);
    return await file.getSignedUrl({
      action: 'read',
      expires: moment().add(1, 'days').format('MM-DD-YYYY'),
      extensionHeaders: {
        'Access-Control-Allow-Origin': '*',
      },
    });
  }
  async deleteFile(filename: string) {
    const file = this.app.storage().bucket().file(filename);
    try {
      return await file.delete();
    } catch (error) {
      throw errorService.externalRequestFailed(error.message);
    }
  }
}
