import { BaseCrudService } from '@core/adapter/mongo/crud/baseCrudService';
import { UserModel, UserDocument } from '@models/user.model';
import { tokenService } from '@services/token';
import { ICrudOption } from '@core/interfaces';
export class UserService extends BaseCrudService<UserDocument> {
  constructor() {
    super(UserModel);
    console.log('contructor UserService');
  }
  getToken(user: UserDocument) {
    return tokenService.createUserToken(user);
  }
  async update(params: UserDocument, option: ICrudOption = {}) {
    const query = this.model.findOneAndUpdate(option.filter, params, {
      new: true,
    });
    this.bucket.cache.flushAll();
    return await this.exec<UserDocument>(query);
  }
}
