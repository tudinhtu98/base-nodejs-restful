import { BaseCrudService } from '@core/adapter/mongo/crud/baseCrudService';
import { SettingGroupModel, SettingGroupDocument } from '@models/settingGroup.model';

export class SettingGroupService extends BaseCrudService<SettingGroupDocument> {
  constructor() {
    super(SettingGroupModel);
    console.log('contructor SettingGroupService');
  }
}
