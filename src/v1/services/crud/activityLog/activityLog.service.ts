import { BaseCrudService } from '@core/adapter/mongo/crud/baseCrudService';
import { ActivityLogModel, ActivityLogDocument } from '@models/activityLog.model';
import { ON_TRACKING_ACTIVITY } from './activityLog.event';

export class ActivityLogService extends BaseCrudService<ActivityLogDocument> {
  constructor() {
    super(ActivityLogModel);
    console.log('contructor ActivityLogService');
    ON_TRACKING_ACTIVITY.subscribe((activity) => {
      let { message, user } = activity;
      console.log(' message, user', message, user);
      new ActivityLogModel({
        user,
        message,
        timestamp: new Date(),
      }).save();
    });
  }
}
