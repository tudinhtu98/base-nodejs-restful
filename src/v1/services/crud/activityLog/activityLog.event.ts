
import { Subject } from 'rxjs';

interface Activity {
    user?: string;
    message?: string;
}
export const ON_TRACKING_ACTIVITY = new Subject<Activity>();