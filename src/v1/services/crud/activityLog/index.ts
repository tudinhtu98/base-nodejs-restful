import { ActivityLogService } from "./activityLog.service";

const activityLogService = new ActivityLogService();

export { activityLogService, ActivityLogService }