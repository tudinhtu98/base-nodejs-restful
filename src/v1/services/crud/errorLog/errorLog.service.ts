import moment = require('moment');

import { BaseCrudService } from '@core/adapter/mongo/crud/baseCrudService';
import { utilService } from '@core/services';
import { ErrorLogDocument, ErrorLogModel } from '@models/errorLog.model';
import { ON_TRACKING_ERROR } from './errorLog.event';

export class ErrorLogService extends BaseCrudService<ErrorLogDocument> {
  constructor() {
    super(ErrorLogModel);
    console.log('contructor ErrorLogService');
    ON_TRACKING_ERROR.subscribe((error) => {
      const date = moment().startOf('date').toDate();
      const hash = utilService.hash(error.message + error.stack + date.toString());
      this.model
        .findOneAndUpdate(
          { hash },
          {
            $inc: { count: 1 },
            $setOnInsert: {
              hash: hash,
              message: error.message,
              name: error.name,
              stack: error.stack ? error.stack.split('\n') : [],
              date: date,
            },
          },
          { upsert: true }
        )
        .exec();
    });
  }
}
