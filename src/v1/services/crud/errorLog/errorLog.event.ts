import { Subject } from 'rxjs';

export const ON_TRACKING_ERROR = new Subject<Error>();
