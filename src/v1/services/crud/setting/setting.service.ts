import { BaseCrudService } from '@core/adapter/mongo/crud/baseCrudService';
import { SettingModel, SettingDocument } from '@models/setting.model';

export class SettingService extends BaseCrudService<SettingDocument> {
  constructor() {
    super(SettingModel);
    console.log('contructor SettingService');
  }
}
