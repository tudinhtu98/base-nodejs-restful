export const ROLES = {
    MEMBER: 'member',
    DOCTOR: 'doctor',
    EDITOR: 'editor'
}

export const ROLES_EDITOR = ['editor'];
export const ROLES_EDITOR_DOCTOR = ['editor', 'doctor'];
export const ROLES_EDITOR_DOCTOR_MEMBER = ['editor', 'doctor', 'member'];