import dotenv from 'dotenv';
import { DevelopmentConfig } from './development';
import { ProductionConfig } from './production';
dotenv.config();

const developmentConfig = new DevelopmentConfig();
const productionConfig = new ProductionConfig();

function getConfig(environment: string) {
    if (environment === 'development') {
        return developmentConfig;
    } else if (environment === 'production') {
        return productionConfig;
    } else {
        return developmentConfig;
    }
}
export const config = getConfig(process.env.NODE_ENV || 'development');