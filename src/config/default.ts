export class DefaultConfig {
  name = 'Postol Service';
  protocol = 'http';
  host = 'localhost';
  port = process.env['PORT'] || '5050';
  secret = process.env['SECRET'] || 'hellokitty';
  domain = 'http://localhost:5050';
  timezone = '+07:00';
  debug = true;
  telegramToken = process.env.TELEGRAM_TOKEN || '';
  telegramGroupId = -346883110;

  // Adapter
  pageSize = 20;

  // MongoDB
  mgConnection =
    process.env['MG_CONNECTION'] || 'mongodb+srv://user:pass@localhost/maindb?retryWrites=true';

  // SharingOrder
  shippingFee = 30000;
  serviceFee = 10000;

  //
  billing = {
    host: process.env['BILLING_HOST'] || 'https://billsb.mcom.app',
    vnpay: {
      key:
        process.env['BILLING_VNPAY_KEY'] ||
        'AK|5f0fde24e9416c002fc98eb9|eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwYXlsb2FkIjp7fSwicm9sZSI6ImFwaV9rZXkiLCJleHAiOiIyMDIxLTA3LTE2VDA0OjU3OjA4LjUwOVoifQ.F9Mg3j1tjpjLbhXzTARHlHesYdMghfWb1uHTtDONrzw',
      secret: process.env['BILLING_VNPAY_SECRET'] || 'fspyt7ufjr',
    },
    momo: {
      key:
        process.env['BILLING_MOMO_KEY'] ||
        'AK|5f0fda27e9416c002fc98eb6|eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwYXlsb2FkIjp7fSwicm9sZSI6ImFwaV9rZXkiLCJleHAiOiIyMDIxLTA3LTE2VDA0OjQwOjA3LjMzNloifQ.FCA8k5L374GwG7iukiCYQOLoOQh0RaSX4bPmLGi6GB4',
      secret: process.env['BILLING_MOMO_SECRET'] || 'n4e6y1tv7a',
    },
  };
}
