import swaggerJSDoc from 'swagger-jsdoc';
import path from 'path';
import { config } from './index';
import { swaggerTheme } from './swaggerTheme';

const swaggerDefinition = {
  openapi: '3.0.1',
  info: {
    title: 'Vai Luc Ngan', // Title of the documentation
    version: '1.0.0', // Version of the app
    description: 'Document RestAPI', // short description of the app
    termsOfService: 'http://swagger.io/terms',
    contact: {
      email: 'apiteam@swagger.io',
    },
  },
  servers: [
    {
      url: `http://localhost:${config.port}/api/v1`,
      description: 'Local (Development) Server',
    },
    // {
    //   url: `https://vien-cay-lua.mcom.app/api/v1`,
    //   description: 'Development Server',
    // }
    {
      url: `https://vai-luc-ngan.mcom.app/api/v1`,
      description: 'Development Server',
    },
  ],
};

const options = {
  // import swaggerDefinitions
  swaggerDefinition,
  // path to the API docs
  apis: [path.join(__dirname, '/../../docs/**/*.yaml')],
};

const swaggerSpec = swaggerJSDoc(options);

export { swaggerSpec, swaggerTheme };
